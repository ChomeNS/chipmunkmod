package land.chipmunk.chipmunkmod.mixin;

import net.minecraft.client.particle.ElderGuardianAppearanceParticle;
import net.minecraft.client.particle.Particle;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(ElderGuardianAppearanceParticle.Factory.class)
public class ElderGuardianAppearanceParticleMixin {
    @Inject(
            method = "createParticle(Lnet/minecraft/particle/SimpleParticleType;Lnet/minecraft/client/world/ClientWorld;DDDDDD)Lnet/minecraft/client/particle/Particle;",
            at = @At("HEAD"),
            cancellable = true)
    private void createParticle(final CallbackInfoReturnable<Particle> cir) {
        cir.setReturnValue(null);
    }
}
