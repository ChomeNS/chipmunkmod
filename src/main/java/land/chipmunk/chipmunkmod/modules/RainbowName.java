package land.chipmunk.chipmunkmod.modules;

import com.mojang.authlib.GameProfile;
import land.chipmunk.chipmunkmod.util.ColorUtilities;


import land.chipmunk.chipmunkmod.util.RandomUtilities;
import land.chipmunk.chipmunkmod.util.UUIDUtilities;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.TextColor;
import net.kyori.adventure.text.serializer.gson.GsonComponentSerializer;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.network.ClientPlayNetworkHandler;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class RainbowName {
    private final MinecraftClient client;

    public static final RainbowName INSTANCE = new RainbowName(MinecraftClient.getInstance());

    private final Random random = new Random();

    public boolean enabled = false;
    private Timer timer = null;

    private String team;
    public String displayName;
    private int startHue = 0;

    public void init () {
        final TimerTask task = new TimerTask() {
            public void run () {
                tick();
            }
        };

        if (timer != null) cleanup();

        timer = new Timer();
        timer.schedule(task, 0, 50);
    }

    public void enable () {
        final ClientPlayNetworkHandler networkHandler = client.getNetworkHandler();
        final GameProfile profile = networkHandler.getProfile();

        final String username = RandomUtilities.emptyUsername(random);
        team = RandomUtilities.randomString(random, RandomUtilities.TEAM_ALLOWED_CHARS, 16);
        final String selfSelector = UUIDUtilities.selector(profile.getId());

        networkHandler.sendChatCommand("extras:username " + username);
        CommandCore.INSTANCE.run("minecraft:team add " + team);
        CommandCore.INSTANCE.run("minecraft:team join " + team + " " + selfSelector);

        enabled = true;
    }

    public void disable () {
        final ClientPlayNetworkHandler networkHandler = client.getNetworkHandler();
        final GameProfile profile = networkHandler.getProfile();

        CommandCore.INSTANCE.run("essentials:nick " + profile.getId() + " off");
        CommandCore.INSTANCE.run("minecraft:team remove " + team);
        networkHandler.sendChatCommand("extras:username " + profile.getName());
        enabled = false;
    }

    public RainbowName (MinecraftClient client) {
        this.client = client;
        this.displayName = client.getSession().getUsername();
    }

    private void tick () {
        try {
            final ClientPlayNetworkHandler networkHandler = client.getNetworkHandler();

            if (networkHandler == null) {
                cleanup();
                return;
            }

            if (!enabled) return;

            final GameProfile profile = networkHandler.getProfile();
            int hue = startHue;
            int increment = (int) (360.0 / Math.max(displayName.length(), 20));

            Component component = Component.empty();
            StringBuilder essentialsNickname = new StringBuilder();

            for (char character : displayName.toCharArray()) {
                String color = String.format("%06x", ColorUtilities.hsvToRgb(hue, 100, 100));
                component = component.append(Component.text(character).color(TextColor.fromHexString("#" + color)));
                essentialsNickname.append("\u00a7#").append(color).append(character != ' ' ? character : '_');
                hue = (hue + increment) % 360;
            }

            CommandCore.INSTANCE.run("minecraft:team modify " + team + " prefix " + GsonComponentSerializer.gson().serialize(component));
            CommandCore.INSTANCE.run("essentials:nick " + profile.getId() + " " + essentialsNickname);

            startHue = (startHue + increment) % 360;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void cleanup () {
        if (timer == null) return;

        timer.cancel();
        timer.purge();
        timer = null;
    }
}
