package land.chipmunk.chipmunkmod.config;

import net.kyori.adventure.text.Component;
import net.minecraft.util.math.BlockBox;
import net.minecraft.util.math.BlockPos;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.configurate.objectmapping.ConfigSerializable;

@ConfigSerializable
public class Configuration {
    public CommandManager commands = new CommandManager();
    public CommandCore core = new CommandCore();
    public Bots bots = new Bots();
    public CustomChat customChat = new CustomChat();
    public String autoSkinUsername = "off";

    @ConfigSerializable
    public static class CommandManager {
        public String prefix = ".";
    }

    @ConfigSerializable
    public static class CommandCore {
        public BlockBox relativeArea = BlockBox.create(new BlockPos(0, 0, 0), new BlockPos(15, 0, 15));
    }

    @ConfigSerializable
    public static class Bots {
        public BotInfo hbot = new BotInfo("#", null);
        public BotInfo sbot = new BotInfo(":", null);
        public BotInfo chipmunk = new BotInfo("'", null);
        public ChomeNSBotInfo chomens = new ChomeNSBotInfo("*", null, null, null);
        public BotInfo fnfboyfriend = new BotInfo("~", null);
        public BotInfo nbot = new BotInfo("?", null);
        public BotInfo kittycorp = new BotInfo("^", null);
        public TestBotInfo testbot = new TestBotInfo("-", null);
    }

    @ConfigSerializable
    public static class ChomeNSBotInfo {
        public String prefix;
        public @Nullable String key;
        public @Nullable String authKey;
        public @Nullable String formatKey;

        public ChomeNSBotInfo() {
        }

        public ChomeNSBotInfo(String prefix, @Nullable String key, @Nullable String authKey, @Nullable String formatKey) {
            this.prefix = prefix;
            this.key = key;
            this.authKey = authKey;
            this.formatKey = formatKey;
        }
    }

    @ConfigSerializable
    public static class TestBotInfo {
        public String prefix;
        public @Nullable String webhookUrl;

        public TestBotInfo() {
        }

        public TestBotInfo(String prefix, @Nullable String webhookUrl) {
            this.prefix = prefix;
            this.webhookUrl = webhookUrl;
        }
    }

    @ConfigSerializable
    public static class BotInfo {
        public String prefix;
        public @Nullable String key;

        public BotInfo() {
        }

        public BotInfo(String prefix, @Nullable String key) {
            this.prefix = prefix;
            this.key = key;
        }
    }

    @ConfigSerializable
    public static class CustomChat {
        public @NotNull Component format =
                Component.translatable("chat.type.text",
                        Component.selector("@s"),
                        Component.text("MESSAGE")
                );
    }
}
