package land.chipmunk.chipmunkmod.config.migration;

import org.spongepowered.configurate.transformation.ConfigurationTransformation;

import java.util.function.Supplier;

public interface ConfigMigration {
    int version();

    ConfigurationTransformation create();

    static ConfigMigration from(final int version, final Supplier<ConfigurationTransformation> supplier) {
        return new ConfigMigrationImpl(version, supplier);
    }

    record ConfigMigrationImpl(int version, Supplier<ConfigurationTransformation> supplier) implements ConfigMigration {
        @Override
        public ConfigurationTransformation create() {
            return this.supplier.get();
        }
    }
}
