package land.chipmunk.chipmunkmod.song;


import net.minecraft.util.math.Vec3d;

public class Note implements Comparable<Note> {
    public Instrument instrument;
    public double pitch;
    public float volume;
    public long time;
    public Vec3d position;

    public Note(Instrument instrument, double pitch, float volume, long time, Vec3d position) {
        this.instrument = instrument;
        this.pitch = pitch;
        this.volume = volume;
        this.time = time;
        this.position = position;
    }

    @Override
    public int compareTo(Note other) {
        return Long.compare(time, other.time);
    }
}
