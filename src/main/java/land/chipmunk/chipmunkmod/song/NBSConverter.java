package land.chipmunk.chipmunkmod.song;

import net.minecraft.util.math.Vec3d;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;

public class NBSConverter {
    public static Instrument[] instrumentIndex = new Instrument[]{
            Instrument.HARP,
            Instrument.BASS,
            Instrument.BASEDRUM,
            Instrument.SNARE,
            Instrument.HAT,
            Instrument.GUITAR,
            Instrument.FLUTE,
            Instrument.BELL,
            Instrument.CHIME,
            Instrument.XYLOPHONE,
            Instrument.IRON_XYLOPHONE,
            Instrument.COW_BELL,
            Instrument.DIDGERIDOO,
            Instrument.BIT,
            Instrument.BANJO,
            Instrument.PLING,
    };

    private static class NBSNote {
        public int tick;
        public short layer;
        public byte instrument;
        public byte key;
        public byte velocity = 100;
        public byte panning = 100;
        public short pitch = 0;
    }

    private static class NBSLayer {
        public String name;
        public byte lock = 0;
        public byte volume;
        public byte stereo = 100;
    }

    private static class NBSCustomInstrument {
        public String name;
        public String file;
        public byte pitch = 0;
        public boolean key = false;
    }

    public static Song getSongFromBytes(byte[] bytes, String fileName) throws IOException {
        ByteBuffer buffer = ByteBuffer.wrap(bytes);
        buffer.order(ByteOrder.LITTLE_ENDIAN);

        short songLength = 0;
        byte format = 0;
        byte vanillaInstrumentCount = 0;
        songLength = buffer.getShort(); // If it's not 0, then it uses the old format
        if (songLength == 0) {
            format = buffer.get();
        }

        if (format >= 1) {
            vanillaInstrumentCount = buffer.get();
        }
        if (format >= 3) {
            songLength = buffer.getShort();
        }

        short layerCount = buffer.getShort();
        String songName = getString(buffer, bytes.length);
        String songAuthor = getString(buffer, bytes.length);
        String songOriginalAuthor = getString(buffer, bytes.length);
        String songDescription = getString(buffer, bytes.length);
        short tempo = buffer.getShort();
        byte autoSaving = buffer.get();
        byte autoSavingDuration = buffer.get();
        byte timeSignature = buffer.get();
        int minutesSpent = buffer.getInt();
        int leftClicks = buffer.getInt();
        int rightClicks = buffer.getInt();
        int blocksAdded = buffer.getInt();
        int blocksRemoved = buffer.getInt();
        String origFileName = getString(buffer, bytes.length);

        byte loop = 0;
        byte maxLoopCount = 0;
        short loopStartTick = 0;
        if (format >= 4) {
            loop = buffer.get();
            maxLoopCount = buffer.get();
            loopStartTick = buffer.getShort();
        }

        ArrayList<NBSNote> nbsNotes = new ArrayList<>();
        short tick = -1;
        while (true) {
            int tickJumps = buffer.getShort();
            if (tickJumps == 0) break;
            tick += tickJumps;

            short layer = -1;
            while (true) {
                int layerJumps = buffer.getShort();
                if (layerJumps == 0) break;
                layer += layerJumps;
                NBSNote note = new NBSNote();
                note.tick = tick;
                note.layer = layer;
                note.instrument = buffer.get();
                note.key = buffer.get();
                if (format >= 4) {
                    note.velocity = buffer.get();
                    note.panning = buffer.get();
                    note.pitch = buffer.getShort();
                }
                nbsNotes.add(note);
            }
        }

        ArrayList<NBSLayer> nbsLayers = new ArrayList<>();
        if (buffer.hasRemaining()) {
            for (int i = 0; i < layerCount; i++) {
                NBSLayer layer = new NBSLayer();
                layer.name = getString(buffer, bytes.length);
                if (format >= 4) {
                    layer.lock = buffer.get();
                }
                layer.volume = buffer.get();
                if (format >= 2) {
                    layer.stereo = buffer.get();
                }
                nbsLayers.add(layer);
            }
        }

        ArrayList<NBSCustomInstrument> customInstruments = new ArrayList<>();
        if (buffer.hasRemaining()) {
            byte customInstrumentCount = buffer.get();
            for (int i = 0; i < customInstrumentCount; i++) {
                NBSCustomInstrument customInstrument = new NBSCustomInstrument();
                customInstrument.name = getString(buffer, bytes.length);
                customInstrument.file = getString(buffer, bytes.length);
                customInstrument.pitch = buffer.get();
                customInstrument.key = buffer.get() == 0 ? false : true;
                customInstruments.add(customInstrument);
            }
        }

        Song song = new Song(songName.trim().length() > 0 ? songName : fileName);
        if (loop > 0) {
            song.looping = true;
            song.loopPosition = getMilliTime(loopStartTick, tempo);
            song.loopCount = maxLoopCount;
        }
        for (NBSNote note : nbsNotes) {
            Instrument instrument;
            double key;
            if (note.instrument < instrumentIndex.length) {
                instrument = instrumentIndex[note.instrument];

                key = (double) ((note.key * 100) + note.pitch) / 100;
            } else {
                int index = note.instrument - instrumentIndex.length;

                if (index >= customInstruments.size()) continue;

                NBSCustomInstrument customInstrument = customInstruments.get(index);
                instrument = Instrument.of(customInstrument.name);

                key = (note.key) + (customInstrument.pitch + (double) note.pitch / 100);
            }

            byte layerVolume = 100;
            if (nbsLayers.size() > note.layer) {
                layerVolume = nbsLayers.get(note.layer).volume;
            }

            while (key < 33) key += 12;
            while (key > 57) key -= 12;

            double pitch = key - 33;

            final int layerStereo = Byte.toUnsignedInt(nbsLayers.get(note.layer).stereo);
            final int notePanning = Byte.toUnsignedInt(note.panning);

            double value;

            if (layerStereo == 100 && notePanning != 100) value = notePanning;
            else if (notePanning == 100 && layerStereo != 100) value = layerStereo;
            else value = (double) (layerStereo + notePanning) / 2;

            double x;

            if (value > 100) x = (value - 100) / -100;
            else if (value == 100) x = 0;
            else x = ((value - 100) * -1) / 100;

            final Vec3d position = new Vec3d(x, 0 ,0);

            song.add(new Note(instrument, pitch, (float) note.velocity * (float) layerVolume / 10000f, getMilliTime(note.tick, tempo), position));
        }

        song.length = song.get(song.size() - 1).time + 50;

        return song;
    }

    private static String getString(ByteBuffer buffer, int maxSize) throws IOException {
        int length = buffer.getInt();
        if (length > maxSize) {
            throw new IOException("String is too large");
        }
        byte arr[] = new byte[length];
        buffer.get(arr, 0, length);
        return new String(arr);
    }

    private static int getMilliTime(int tick, int tempo) {
        return 1000 * tick * 100 / tempo;
    }
}
