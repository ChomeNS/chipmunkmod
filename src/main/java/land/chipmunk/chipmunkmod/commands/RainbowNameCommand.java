package land.chipmunk.chipmunkmod.commands;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.context.CommandContext;
import land.chipmunk.chipmunkmod.modules.RainbowName;
import net.fabricmc.fabric.api.client.command.v2.FabricClientCommandSource;
import net.minecraft.text.Text;

import static com.mojang.brigadier.arguments.BoolArgumentType.bool;
import static com.mojang.brigadier.arguments.BoolArgumentType.getBool;
import static com.mojang.brigadier.arguments.StringArgumentType.getString;
import static com.mojang.brigadier.arguments.StringArgumentType.greedyString;
import static land.chipmunk.chipmunkmod.command.CommandManager.argument;
import static land.chipmunk.chipmunkmod.command.CommandManager.literal;

public class RainbowNameCommand {
    public static void register (CommandDispatcher<FabricClientCommandSource> dispatcher) {
        dispatcher.register(
                literal("rainbowname")
                        .then(
                                literal("enabled")
                                        .then(
                                                argument("boolean", bool())
                                                        .executes(RainbowNameCommand::enabled)
                                        )
                        )
                        .then(
                                literal("setName")
                                        .then(
                                                argument("name", greedyString())
                                                        .executes(RainbowNameCommand::setName)
                                        )
                        )
        );
    }

    public static int enabled (CommandContext<FabricClientCommandSource> context) {
        final FabricClientCommandSource source = context.getSource();

        final boolean bool = getBool(context, "boolean");

        if (bool) {
            RainbowName.INSTANCE.enable();
            source.sendFeedback(Text.literal("Rainbow name is now enabled"));
        } else {
            RainbowName.INSTANCE.disable();
            source.sendFeedback(Text.literal("Rainbow name is now disabled"));
        }

        return Command.SINGLE_SUCCESS;
    }

    public static int setName (CommandContext<FabricClientCommandSource> context) {
        final FabricClientCommandSource source = context.getSource();

        final String name = getString(context, "name");

        RainbowName.INSTANCE.displayName = name;

        source.sendFeedback(Text.literal("Set the display name to: " + name));

        return Command.SINGLE_SUCCESS;
    }
}
