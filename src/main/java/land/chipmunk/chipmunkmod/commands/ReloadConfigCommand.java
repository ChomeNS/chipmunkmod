package land.chipmunk.chipmunkmod.commands;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.context.CommandContext;
import land.chipmunk.chipmunkmod.ChipmunkMod;
import land.chipmunk.chipmunkmod.modules.CommandCore;
import net.fabricmc.fabric.api.client.command.v2.FabricClientCommandSource;
import net.minecraft.text.Text;

import java.io.IOException;

import static land.chipmunk.chipmunkmod.command.CommandManager.literal;

public class ReloadConfigCommand {
    public static void register (CommandDispatcher<FabricClientCommandSource> dispatcher) {
        dispatcher.register(
                literal("reloadconfig")
                        .executes(ReloadConfigCommand::reload)
        );
    }

    public static int reload(CommandContext<FabricClientCommandSource> context) {
        final FabricClientCommandSource source = context.getSource();

        try {
            ChipmunkMod.CONFIG = ChipmunkMod.loadConfig();
            CommandCore.INSTANCE.reloadRelativeArea();

            source.sendFeedback(Text.literal("Successfully reloaded the config"));
        } catch (IOException e) {
            source.sendError(Text.literal("Could not load config, check the logs for stacktrace"));
            e.printStackTrace();
        }

        return Command.SINGLE_SUCCESS;
    }
}
