package land.chipmunk.chipmunkmod.util;

import net.minecraft.client.MinecraftClient;
import net.minecraft.client.network.ClientPlayNetworkHandler;

public class ServerUtilities {
    public static boolean serverHasCommand (String name) {
        final MinecraftClient client = MinecraftClient.getInstance();
        final ClientPlayNetworkHandler networkHandler = client.getNetworkHandler();

        if (networkHandler == null) return false;

        return networkHandler.getCommandDispatcher().getRoot().getChild(name) != null;
    }
}
