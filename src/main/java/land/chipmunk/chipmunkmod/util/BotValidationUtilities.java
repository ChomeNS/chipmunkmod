package land.chipmunk.chipmunkmod.util;

import com.mojang.brigadier.Command;
import land.chipmunk.chipmunkmod.ChipmunkMod;
import land.chipmunk.chipmunkmod.config.Configuration;
import land.chipmunk.chipmunkmod.modules.Chat;
import land.chipmunk.chipmunkmod.modules.custom_chat.CustomChat;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.network.ClientPlayNetworkHandler;
import net.minecraft.client.network.ClientPlayerEntity;
import org.apache.commons.codec.binary.Hex;

import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

public class BotValidationUtilities {
    public static int hbot (String command) throws RuntimeException {
        final Configuration.BotInfo info = ChipmunkMod.CONFIG.bots.hbot;
        final MinecraftClient client = MinecraftClient.getInstance();
        final ClientPlayNetworkHandler networkHandler = client.getNetworkHandler();

        final String prefix = info.prefix;
        final String key = info.key;
        if (key == null) throw new RuntimeException("The key of the bot is unspecified (null), did you incorrectly add it to your config?");

        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            String time = String.valueOf(System.currentTimeMillis() / 10000);
            String input = prefix + command.replaceAll("&[0-9a-fklmnor]", "") + ";" + client.player.getUuidAsString() + ";" + time + ";" + key;
            byte[] hash = md.digest(input.getBytes(StandardCharsets.UTF_8));
            BigInteger bigInt = new BigInteger(1, Arrays.copyOfRange(hash, 0, 4));
            String stringHash = bigInt.toString(Character.MAX_RADIX);

            Chat.sendChatMessage(prefix + command + " " + stringHash, true);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return Command.SINGLE_SUCCESS;
    }

    public static int sbot (String command) throws RuntimeException {
        final Configuration.BotInfo info = ChipmunkMod.CONFIG.bots.sbot;
        final MinecraftClient client = MinecraftClient.getInstance();
        final ClientPlayNetworkHandler networkHandler = client.getNetworkHandler();

        final String prefix = info.prefix;
        final String key = info.key;
        if (key == null) throw new RuntimeException("The key of the bot is unspecified (null), did you incorrectly add it to your config?");

        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            String time = String.valueOf(System.currentTimeMillis() / 20000);
            String input = prefix + command.replaceAll("&[0-9a-fklmnorx]", "") + ";" + client.player.getName().getString() + ";" + time + ";" + key;
            byte[] hash = md.digest(input.getBytes(StandardCharsets.UTF_8));
            BigInteger bigInt = new BigInteger(1, Arrays.copyOfRange(hash, 0, 4));
            String stringHash = bigInt.toString(Character.MAX_RADIX);

            Chat.sendChatMessage(prefix + command + " " + stringHash, true);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return Command.SINGLE_SUCCESS;
    }

    public static int chomens (String command) throws RuntimeException {
        final Configuration.ChomeNSBotInfo info = ChipmunkMod.CONFIG.bots.chomens;

        final MinecraftClient client = MinecraftClient.getInstance();

        final ClientPlayerEntity player = client.player;

        final String prefix = info.prefix;
        final String key = info.key;
        if (key == null) throw new RuntimeException("The key of the bot is unspecified (null), did you incorrectly add it to your config?");

        try {
            String[] arguments = command.split(" ");

            MessageDigest md = MessageDigest.getInstance("SHA-256");
            String time = String.valueOf(System.currentTimeMillis() / 5_000);
            String input = client.player.getUuidAsString() + arguments[0] + time + key;
            byte[] hash = md.digest(input.getBytes(StandardCharsets.UTF_8));
            String stringHash = new String(Hex.encodeHex(hash)).substring(0, 16);

            final boolean shouldSectionSign = CustomChat.INSTANCE.enabled && player.hasPermissionLevel(2) && player.isCreative();

            if (shouldSectionSign) {
                stringHash = String.join("",
                        Arrays.stream(stringHash.split(""))
                            .map((letter) -> "§" + letter)
                            .toArray(String[]::new)
                );
            }

            final String[] restArguments = Arrays.copyOfRange(arguments, 1, arguments.length);

            final String toSend = prefix +
                    arguments[0] +
                    " " +
                    stringHash +
                    (shouldSectionSign ? "§r" : "") +
                    " " +
                    String.join(" ", restArguments);

            Chat.sendChatMessage(toSend);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return Command.SINGLE_SUCCESS;
    }

    public static int fnfboyfriend (String command) {
        try {
            final String prefix = ChipmunkMod.CONFIG.bots.fnfboyfriend.prefix;

            String[] arguments = command.split(" ");

            long currentTime = System.currentTimeMillis() / 2000;
            final String key = ChipmunkMod.CONFIG.bots.fnfboyfriend.key;
            if (key == null) throw new RuntimeException("The key of the bot is unspecified (null), did you incorrectly add it to your config?");
            String input = currentTime + key;
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(input.getBytes());
            StringBuilder hexString = new StringBuilder();
            for (byte b : hash) {
                String hex = Integer.toHexString(0xff & b);
                if (hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }

            final String[] restArguments = Arrays.copyOfRange(arguments, 1, arguments.length);

            final String result = hexString.substring(0, 16);

            Chat.sendChatMessage(prefix + arguments[0] + " " + result + " " + String.join(" ", restArguments));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return Command.SINGLE_SUCCESS;
    }

    public static int nbot (String command) throws RuntimeException {
        final Configuration.BotInfo info = ChipmunkMod.CONFIG.bots.nbot;

        final MinecraftClient client = MinecraftClient.getInstance();

        final String prefix = info.prefix;
        final String key = info.key;
        if (key == null) throw new RuntimeException("The key of the bot is unspecified (null), did you incorrectly add it to your config?");

        try {
            String[] arguments = command.split(" ");

            MessageDigest md = MessageDigest.getInstance("SHA-256");

            String time = String.valueOf(System.currentTimeMillis() / 5_000);
            String input = arguments[0].replaceAll("&[0-9a-fklmnor]", "") + ";" + client.player.getUuidAsString() + ";" + time + ";" + key;

            md.update(input.getBytes(StandardCharsets.UTF_8));

            byte[] hash = md.digest();

            ByteBuffer buffer = ByteBuffer.wrap(hash, 0, 4);
            long bigInt = (buffer.getInt() & 0xFFFFFFFFL);;
            String stringHash = Long.toString(bigInt, 36);

            final String[] restArguments = Arrays.copyOfRange(arguments, 1, arguments.length);

            final String toSend = prefix +
                    arguments[0] +
                    " " +
                    stringHash +
                    " " +
                    String.join(" ", restArguments);

            Chat.sendChatMessage(toSend, true);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return Command.SINGLE_SUCCESS;
    }

    public static int kittycorp (String command) throws RuntimeException {
        final Configuration.BotInfo info = ChipmunkMod.CONFIG.bots.kittycorp;
        final ClientPlayNetworkHandler networkHandler = MinecraftClient.getInstance().getNetworkHandler();

        final String prefix = info.prefix;
        final String key = info.key;
        if (key == null) throw new RuntimeException("The key of the bot is unspecified (null), did you incorrectly add it to your config?");

        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            String time = String.valueOf(System.currentTimeMillis() / 10000);
            String input = prefix + command.replaceAll("&[0-9a-fklmnorx]", "") + ";" + time + ";" + key;
            byte[] hash = md.digest(input.getBytes(StandardCharsets.UTF_8));
            BigInteger bigInt = new BigInteger(1, Arrays.copyOfRange(hash, 0, 4));
            String stringHash = bigInt.toString(Character.MAX_RADIX);

            Chat.sendChatMessage(prefix + command + " " + stringHash, true);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return Command.SINGLE_SUCCESS;
    }
}
