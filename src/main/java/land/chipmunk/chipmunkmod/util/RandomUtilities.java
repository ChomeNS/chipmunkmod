package land.chipmunk.chipmunkmod.util;

import java.util.Random;

public final class RandomUtilities {
    // https://github.com/kaboomserver/extras/blob/master/src/main/java/pw/kaboom/extras/util/Utility.java#L32
    public static final char[] LEGACY_STYLE_CODES = "0123456789abcdefklmnorx".toCharArray();
    public static final char[] TEAM_ALLOWED_CHARS = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz_-.+"
            .toCharArray();

    public static String emptyUsername(final Random random) {
        final char[] buf = new char[16];

        for (int i = 0; i < 16; i += 2) {
            final int j = random.nextInt(LEGACY_STYLE_CODES.length);

            buf[i] = '&';
            buf[i + 1] = LEGACY_STYLE_CODES[j];
        }

        return new String(buf);
    }

    public static String randomString(final Random random, final char[] charset, final int length) {
        final char[] buf = new char[length];

        for (int i = 0; i < length; i++) {
            final int j = random.nextInt(charset.length);
            buf[i] = charset[j];
        }

        return new String(buf);
    }
}
