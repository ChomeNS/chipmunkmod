package land.chipmunk.chipmunkmod.data;

import land.chipmunk.chipmunkmod.ChipmunkMod;
import land.chipmunk.chipmunkmod.util.TextUtilities;
import net.minecraft.text.Text;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Objects;

public record ChomeNSBotCommand(String name, TrustLevel trustLevel, List<String> aliases) {
    public static @Nullable ChomeNSBotCommand fromText(final Text component) {
        final String name = TextUtilities.plainOrNull(component);
        if (name == null) return null;

        final List<Text> children = component.getSiblings();
        if (children.size() < 2) return null; // must have at least trust level and alias boolean

        final TrustLevel trustLevel = TrustLevel.fromText(children.getFirst());
        if (trustLevel == null) return null;

        final String hasAliasesString = TextUtilities.plainOrNull(children.get(1));
        if (hasAliasesString == null) return null;

        final boolean hasAliases = Boolean.parseBoolean(hasAliasesString);
        if (!hasAliases) return new ChomeNSBotCommand(
                ChipmunkMod.CONFIG.bots.chomens.prefix + name, trustLevel, List.of());

        final List<String> aliases = children.stream()
                .skip(2)
                .map(TextUtilities::plainOrNull)
                .filter(Objects::nonNull)
                .toList();
        return new ChomeNSBotCommand(
                ChipmunkMod.CONFIG.bots.chomens.prefix + name, trustLevel, aliases);
    }

    public enum TrustLevel {
        PUBLIC,
        TRUSTED,
        ADMIN,
        OWNER;

        public static TrustLevel fromText(final Text component) {
            final String trustLevelString = TextUtilities.plainOrNull(component);
            if (trustLevelString == null) return null;

            try {
                return TrustLevel.valueOf(trustLevelString);
            } catch (final IllegalArgumentException ignored) {
            }

            return null;
        }
    }
}
